function loadLib
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Loading p7889lib...');
if not(libisloaded('p7889lib'))
    [notfound,warnings] = loadlibrary('p7889lib','p7889.h');
    if(~isempty(notfound))
        set(handle.status,'backgroundcolor','r');
        set(handle.status,'string','Loading p7889lib...Error');
        error('Load lib error!');
    end
end
set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Loading lib...Done');
drawnow;
% libfunctions('p7889lib');