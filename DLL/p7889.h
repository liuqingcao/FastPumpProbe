				  // Board Properties and initialisation parameters
#include <windows.h>
#define DLL


typedef struct {
  double fPLL;		  // PLL frequency: 10e9
  double tosc;		  // clock oscillator period (ns): 100.
  long serno;		  // Board serial number
  long version;       // FPGA version
  long ires;		  // board type (speed): 6
  long blocksize;     // blocksize in DWORDs for DMA transfer 
  long dmatimeout;    // timeout for DMA transfer in msec 
  long nodma;	      // if ==1: don't use DMA
  long extclk;		  //         external clock
  long factory;		  //         do factory tests
  long reserved1;	  //  
  long reserved2;     // for future use...
  long reserved3;	  // 
} GENPROPERTY; 

					// Board and FPGA temperatures and voltages
typedef struct {
	double v25;		  // 2.5 V
	double v15;		  // 1.5 V
	double vcc;		  // 3.3 V
	double v50;		  // 5  V
	double v120;	  // 12 V
	double v33;		  // 3.3 V
	double ain2;	  // -12 V
	int tfpga;		  // FPGA temperature (centigrade)
	int tboard;		  // board temperature
	int fan1;		  // function of fan revolution frequency, 
					  // =255 means not operating, =94 max speed
	int dac;		  // controls fan: dac=255 max, dac=120 normal speed
} LM87VAL;

                  // Status
typedef struct {
  long started;         // aquisition status: 1 if running, 0 else
  unsigned long maxval; // Maximum value in spectra
  double runtime;       // running time in seconds
  double totalsum;      // total events
  double roisum;        // events within ROI
  double roirate;       // acquired ROI-events per second
  double ofls;
  double sweeps;        // Number of sweeps
  double stevents;      // Start Events
  double cyclecnt;      // cycles in sequential mode
  double seqcnt;        // number of sequences in sequential mode
  double seqsum;        // accumulated sum for sequential runs
  double cal0;			// Time calibration:
  double cal1;			// time = cal0 + cal1 * channel
} ACQSTATUS;

                  // General Settings
typedef struct {
  unsigned long range;  // spectra length
  long bitshift;        // Binwidth = 2 ^ (bitshift)
  unsigned long roimin; // lower ROI limit
  unsigned long roimax; // upper limit: roimin <= channel < roimax
  long sweepmode;       // sweepmode & 0xF: 0 = normal, 
   						// 1=differential (relative to first stop in sweep)
						// 4=sequential
                        // bit 4: Softw. Start
                        // bit 5: DMA mode
                        // bit 6: Wrap around
                        // bit 7: Start event generation
  						// bit 8: Enable Tag bits
						// bit (9,10): 0=rising, 1=falling, 2=both, 3=both+CFT 
						// bit 11: pulse width
						// bit 12: 6 bits of Sweepcounter in Data
						// bit 13: card ID in data 
  long cycles;          // for sequential mode
  long sequences;
  long syncout;         // sync out; bit 0..7 NIM syncout, bit 8..15 TTL syncout
						// NIM: 0=STOP, 1=START, 
						// TTL (0=,1=..) , NIM (2=,3=..): 
						// 156.25 MHz, 78.125 MHz, PCICLK, 10 MHz, INPUT_ON,
						// WINDOW, HOLD_OFF, TIME[0],... TIME[31],
						// SWEEP[0],...,SWEEP[31]
  long cftfak;          // reserved (1000 * cft factor (t_to_peak / t_after_peak))
  long active;          // 1 for module enabled in system 1
  long dmamode;			// obsolete, DMA mode now set automatically
  double fstchan;       // acquisition delay in units of 6.4 ns
  double holdafter;     // wait time after sweep in units of ns
} GENSETTING;

typedef struct {
  int savedata;			//  bit 0: 1 if auto save after stop
						//  bit 1: write listfile
						//  bit 2: listfile only, no histogram)
  int autoinc;			// 1 if auto increment fielename
  int fmt;              // format type: 0 == ASCII, 1 == binary
  int smpts;
  int caluse;
  char filename[256];	// file name
  char comment0[512];	// string, filled with start time and date
} DATSETTING;

				  // Preset Settings
typedef struct {
  long prena;           // bit 0: realtime preset enabled
						// bit 1: reserved
                        // bit 2: sweep preset enabled
                        // bit 3: ROI preset enabled
						// bit 4: Starts preset enabled
  long swpreset;        // sweep preset value
  double eventpreset;   // ROI preset value
  double timepreset;    // time preset value
} PRESETTING;
  
				  // Input Threshold Settings
typedef struct {
  long dac0;            // bit0..15 DAC0 value (START), bit16: Startpolarity 1=rising
  long dac1;            // bit0..15 DAC1 value (STOP),  bit16: time under threshold for pulse width
} DACSETTING;

				  // Dig IO Settings
typedef struct {
  long digio;           // Use of Dig I/O, GO Line:
                        // bit 0: status dig 0..3
                        // bit 1: Output digval and increment digval after stop
                        // bit 2: Invert polarity
						// (bit 3: Push-Pull output, not possible)
						// bit 8: GOWATCH
						// bit 9: GO High at Start
						// bit 10: GO Low at Stop
  long digval;          // digval=0..255 value for samplechanger
} DIGIOSETTING;

#define DATAPTR   unsigned long*


#ifdef DLL
long WINAPI SetP89(int board, GENPROPERTY *prop, GENSETTING *setting, 
				   PRESETTING *presetting, DACSETTING *dacset, 
				   DIGIOSETTING *digset, DATSETTING *datset);
			// Initialize, set parameters and get board properties
			// Pass NULL for any settings pointer you don't want to set now.
			// Must be called at first to get a handle to the driver

long WINAPI SetDataP89(int board, int allocmode, DATAPTR *data);
			// if allocmode==1 memory will be allocated, 
			// for allocmode==0 allocate memory in your application 
			// (length Setting->range*sizeof(DWORD) or for sequential mode 
			// cycles * range ))
			// and pass pointer to spectra pointer  

long WINAPI SetVBDataP89(int board, DATAPTR data);
			// version for program languages like VB that don't have pointers of pointers 
			// allocate memory in your application 
			// (length Setting->range or for sequential mode cycles * range)
			// and pass pointer to spectra pointer  
			// returns memory size in DWORDs

long WINAPI OpenDatP89(int board, HANDLE *handle, DATAPTR *data);
			// Open a view to the data for allocmode=1
			// Can be used for secondary application accessing the DLL
			// Save handle for closing

long WINAPI CloseDatP89(int board, HANDLE *handle, DATAPTR *data);
			// Close a view to the data for allocmode=1

long WINAPI LeaveP89 (void); // Close everything

long WINAPI GetP89 (int board, GENPROPERTY *prop, GENSETTING *setting, 
				   PRESETTING *presetting, DACSETTING *dacset, 
				   DIGIOSETTING *digset, DATSETTING *datset,
							   LM87VAL *lm87);
			// Get settings, returns the active memory size. 
			// Pass NULL for any settings pointer you are not interested now.					 

long WINAPI SetFanP89(int nDev, int dac);
			// Set Fan speed (max. dac=255, normal =120)

long WINAPI StartP89(int isys, int bcontinue);
			// Start acquisition, using built-in multithreaded FIFO read functions

long WINAPI MyStartP89(int isys, int bcontinue);
			// Start acquisition, MyFifoWatch1 function must be called repeatedly
			// isys is the system number Setting.active (def. == 1)
			// Set bcontinue=1 for continue acquisition 

long WINAPI HaltP89(int isys);
			// Stop acquisition

long WINAPI EraseP89(int board);
			// Clear spectra

long WINAPI StatusP89 (int board, ACQSTATUS *status);
			// gets acquisition status

long WINAPI TriggerP89 (int board);
			// Makes software trigger
int WINAPI MyFifoWatch1(long dataPtr, DWORDLONG *data);
int WINAPI MyFifoWatch2(long dataPtr, DWORDLONG *data);
			// Read FIFO, no multithreading
int WINAPI MyCheckrunning(int nDev);
			// Check if running, returns 0 if sweep preset reached
long WINAPI MyRestart89(int isys);
			// Restart after preset reached, buffers still allocated

long WINAPI fredSetCallBackFunction(void (*newSingleShotCallback)(unsigned long shot , unsigned long channel));

//long WINAPI fredTest(int test);

#else
typedef long (WINAPI *P89SET) (int board, GENPROPERTY *prop, GENSETTING *setting, 
				   PRESETTING *presetting, DACSETTING *dacset, 
				   DIGIOSETTING *digset, DATSETTING *datset);
			// Initialize, set parameters and get board properties
			// Pass NULL for any settings pointer you don't want to set now.
			// Must be called at first to get a handle to the driver

typedef long (WINAPI *P89DATASET) (int board, int allocmode, DATAPTR *data);
			// if allocmode==1 memory will be allocated, 
			// for allocmode==0 allocate memory in your application 
			// (length Setting->range*sizeof(DWORD) or for sequential mode 
			// cycles * range * sizeof(DWORD))
			// and pass pointer to spectra pointer  

typedef long (WINAPI *P89DATAOPEN) (int board, HANDLE *handle, DATAPTR *data);
			// Open a view to the data for allocmode=1
			// Can be used for secondary application accessing the DLL
			// Save handle for closing

typedef long (WINAPI *P89DATACLOSE) (int board, HANDLE *handle, DATAPTR *data);
			// Close a view to the data for allocmode=1

typedef long (WINAPI *P89START) (int isys, int bcontinue);
			// Start acquisition, returns 1 for board not initialized
			// isys is the system number Setting.active (def. == 1)
			// Set bcontinue=1 for continue acquisition 

typedef long (WINAPI *P89HALT) (int isys);
					// Stop acquisition

typedef long (WINAPI *P89ERASE) (int board);
					// Clear spectra

typedef long (WINAPI *P89LEAVE) (void); // Close everything

typedef long (WINAPI *P89GET) (int board, GENPROPERTY *prop, GENSETTING *setting, 
				   PRESETTING *presetting, DACSETTING *dacset, 
				   DIGIOSETTING *digset, DATSETTING *datset,
				   LM87VAL *lm87);
			// Get settings, returns the active memory size. 
			// Pass NULL for any settings pointer you are not interested now.					 

typedef long (WINAPI *P89SETFAN)(int nDev, int dac);
			// Set Fan speed (max. dac=255, normal =120)

typedef long (WINAPI *P89STATUS) (int board, ACQSTATUS *status);
					// gets acquisition status

typedef long (WINAPI *P89TRIGGER) (int board);
					// Makes software trigger
										 
typedef int (WINAPI *MYWATCH) (long dataPtr, DWORDLONG *data);
				
typedef int (WINAPI *MYCHECK) (int nDev);
typedef long (WINAPI *MYRESTART) (int isys);

typedef long (WINAPI  *FREDSETCALLBACKFUNCTION)(void (*newSingleShotCallback)(unsigned long shot , unsigned long channel));

typedef long (WINAPI  *FREDTEST)(int test);

#endif										 
