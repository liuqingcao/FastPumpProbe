close all;
clear all;
clc;
%the global handles for the main interface
global fastSet;
global scanSet;
global mainHandle;
global scanTool;

fastSet = [];
scanSet = [];
scanTool = [];
mainHandle = [];
mainHandle.baseFolder = fullfile('D:','PumpProbe_C60');
mainHandle.autoRef = 1;
% system path
currentFile = mfilename('fullpath');
i = strfind(currentFile,filesep);
mainHandle.systemPath = currentFile(1:i(end)-1);
% add all the subfolders
addpath(genpath(mainHandle.systemPath));
clear currentFile i;

% initial parameters for P7889
fastSet.numBoard = 0;
fastSet.lengthSpectrum = 32768;
fastSet.bitshift = 0;
fastSet.binSize = 2^fastSet.bitshift*0.1;  %ns
fastSet.sweepSum = 5000;
fastSet.startThresholdVolts = 0.5;
fastSet.startEdge = 0;  %0-falling, 1-rising
fastSet.stopThresholdVolts = 0.5;
fastSet.stopEdge = 0;  %0-falling, 1-rising
fastSet.startDelay = 1; %acquisition delay in units of 6.4 ns
fastSet.holdafter = 499.2; %wait time after sweep in units of ns
fastSet.ready = 0;

% initial parameters for scan
scanSet.project = 'test';
scanSet.comment = 'Here is the commnent!';
scanSet.startDelay = -200;
scanSet.endDelay = 200;
scanSet.stepDelay = 10;
scanSet.repetitionRate = 500;
scanSet.zeroPosition = 10;
scanSet.selectTool = 1;
scanSet.lightPeak = 100;
scanSet.keRange = [1,5000];
scanSet.keResolution = 2;
scanSet.tofBinMin = 0;
scanSet.tofBinMax = 0;
scanSet.stepNow = 1;
scanSet.status = 0;
%
%create the GUI
mainGui;
%initilize scan stage
initScanTool;







