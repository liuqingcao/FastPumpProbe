function setPhaseTo(phaseIndex,delayIndex)
global phaseStage;
global scanSettings;
global allHandles;
handle =allHandles.handles;

%move phase stage
phaseStage.SetAbsMovePos(0,scanSettings.phases(phaseIndex));
phaseStage.MoveAbsolute(0,0);

if(phaseIndex==1)
    pause(5);
else
    pause(1);
end
set(handle.delayIndicator, 'String', [num2str(scanSettings.delays(delayIndex),'%6.2f'),'/',num2str(phaseIndex)]);

