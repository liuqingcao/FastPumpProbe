function thorLabInit
global scanTool; % make a global variable so it can be used outside the main
          % function. Useful when you do event handling and sequential move
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Initialize thorlab...');          
%% Create Matlab Figure Container
fpos    = get(0,'DefaultFigurePosition'); % figure default position
fpos(3) = 650; % figure window size;Width
fpos(4) = 450; % Height
 
%if it`s already exist
hshow=findobj('tag','Thorlab');
if(~isempty(hshow))
    figure(hshow);
    return;
end

f = figure('NumberTitle','off', 'Tag', 'Thorlab',...
           'Position', fpos,...
           'Menu','None',...
           'Name','Thorlab');
%% Create ActiveX Controller
scanTool = actxcontrol('MGMOTOR.MGMotorCtrl.1',[20 20 600 400 ], f);

%% Initialize
% Start Control
scanTool.StartCtrl;
 
% Set the Serial Number
SN = 83843711; % put in the serial number of the hardware
set(scanTool,'HWSerialNum', SN);
 
% Indentify the device
% scanTool.Identify;
 
pause(1); % waiting for the GUI to load up;


set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Initialize thorlab...Done');  




