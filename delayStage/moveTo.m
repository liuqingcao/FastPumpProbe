function moveTo(delayTime)
global scanSet;
global scanTool;
global mainHandle;
handle = mainHandle.handles;

set(handle.status,'backgroundcolor','y');
set(handle.status,'string',['Move to ',num2str(pos,'%5.1f'),'...']);   
run = 1;
switch scanSet.selectTool
    case 1 %thorlab
        
    case 2 %pollux
        position = 0.00015*delayTime + scanTool.zeroPosition;
        command = [num2str(position,'%9.6f'),' 1 nm'];
        run = system([scanTool.console,' "/c ',command,'"']);

    case 3 %AS5-CEP
        
end


set(handle.status,'backgroundcolor','g');
set(handle.status,'string',['Move to ',num2str(pos,'%5.1f'),'...Done']);   

