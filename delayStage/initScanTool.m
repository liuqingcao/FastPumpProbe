function initScanTool
global scanSet;

switch scanSet.selectTool
    case 1
        thorLabInit;
    case 2
        polluxInit(1);
    case 3
        as5PhaseInit;
end