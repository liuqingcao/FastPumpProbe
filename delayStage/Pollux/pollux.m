function status = pollux(command)
global polluxSetting;

    status = system([polluxSetting.console,' "/c ',command,'"']);
    if(status)
       h = msgbox(['The interferometer command <',command,'> failed!'],'pollux.m','error');
       uiwait(h);
    end
    pause(0.5);
    
%     %get the position now
%     [status,cmdout] = system([polluxSetting.console,' "/c 1 np"']);
%     if(status)
%        msgbox('get current position wrong','pollux','error'); 
%     end
%     polluxSetting.whereAmI = str2double(char(cmdout(size(cmdout,2)-9:size(cmdout,2)-1)));
