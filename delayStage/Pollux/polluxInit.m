function polluxInit(setZero)
global scanTool;
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Initialize Pollux...');

scanTool.consolePath = fullfile(mainHandle.systemPath,'delayStage','Pollux','console');
scanTool.consoleName = 'Console.exe';
scanTool.consoleAxis = 1;
scanTool.consoleCOM = 2;
scanTool.consoleMode = 128;
scanTool.consoleBT = 19200;
scanTool.console = fullfile(scanTool.consolePath, ...
                        [scanTool.consoleName, ...
                        ' /p',num2str(scanTool.consoleCOM), ...
                        ' /b',num2str(scanTool.consoleBT), ...
                        ' /a',num2str(scanTool.consoleAxis), ...
                        ' /m',num2str(scanTool.consoleMode)]);
                    
                    
%initial the pollux
status = pollux('1 nversion');
if(status)
    set(handle.status,'backgroundcolor','r');
    set(handle.status,'string','Initialize Pollux...Error');
    drawnow;
    return;
end
%set velocity
pollux('10.0000 1 snv');
%set the acceleration
pollux('100.0000 1 sna');

if(setZero)
    set(handle.status,'string','Initialize Pollux...Home');
    %goto the maxmum limitation
    pollux('1 nrm');
    pause(5);
    pollux('1 np');
    %run -50.0 mm
    pollux('-50.000 1 nr');
    pause(5);
    pollux('1 np');
    %set the current position as zero
    pollux('0.000 1 setnpos');
    %move to 0 delay
    moveTo(0);
end
set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Initialize Pollux...Done');









