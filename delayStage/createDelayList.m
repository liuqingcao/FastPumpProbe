function createDelayList
global scanSet;
global fastSet;
global mainHandle;
handle = mainHandle.handles;

scanSet.delayList = [scanSet.startDelay:scanSet.stepDelay:scanSet.endDelay];
scanSet.steps = length(scanSet.delayList);

if(scanSet.steps == 0)
    h=msgbox('Delay steps setting is wrong!','set delay steps','error');
    uiwait(h);
    scanSet.status = 0;
    return;
end

%print information
mainHandle.stepTime = fastSet.sweepSum/scanSet.repetitionRate/60;
str = {['Total steps: ',num2str(scanSet.steps)]; ...
        [num2str(mainHandle.stepTime,'%5.1f'),' minutes per step']; ...
        [num2str(mainHandle.stepTime*scanSet.steps,'%5.1f'),' minutes in total']};
set(handle.scanTime,'String',str);
set(handle.restTime,'String',[num2str(mainHandle.stepTime*scanSet.steps,'%5.1f'),' minutes left']);