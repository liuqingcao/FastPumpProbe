function resetData
global fastSet;
global scanSet;
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Reset data array...');

flightLength = 535; %mm
me = 0.910938970e-30;
q = 1.602176565e-19;
scanSet.tof2ke = 0.5*me/q*1e12*flightLength*flightLength;

%reset data
scanSet.startDate = zeros(scanSet.steps,6);
scanSet.finishDate = zeros(scanSet.steps,6);
%
scanSet.tofCounts = zeros(1,scanSet.steps);
%
scanSet.refTOF = zeros(1,fastSet.lengthSpectrum);
scanSet.newTOF = zeros(1,fastSet.lengthSpectrum);
scanSet.allTOF = zeros(scanSet.steps,fastSet.lengthSpectrum);
%time axis
scanSet.timeAxis = [1:fastSet.lengthSpectrum]*fastSet.binSize;  %ns
scanSet.timeAxisReal = ([1:fastSet.lengthSpectrum]-scanSet.lightPeak)*fastSet.binSize;  %ns
scanSet.timeAxisReal(1:scanSet.lightPeak) = 0;
scanSet.timeAxisKe = scanSet.tof2ke./scanSet.timeAxisReal.^2;
scanSet.timeAxisKe(1:scanSet.lightPeak) = 0;
[~,scanSet.tofBinMin] = min(abs(max(scanSet.keRange)-scanSet.timeAxisKe));
scanSet.tofBinMin = scanSet.tofBinMin-1;
[~,scanSet.tofBinMax] = min(abs(min(scanSet.keRange)-scanSet.timeAxisKe));
%create energy axis
scanSet.keAxis = [min(scanSet.keRange):scanSet.keResolution:max(scanSet.keRange)];
scanSet.keAxisLength = length(scanSet.keAxis);
%
scanSet.refSpec = zeros(1,scanSet.keAxisLength);
scanSet.newSpec = zeros(1,scanSet.keAxisLength);
scanSet.allSpec = zeros(scanSet.steps,scanSet.keAxisLength);


set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Reset data array...Done');
