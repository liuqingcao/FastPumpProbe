function resetFast
global fastSet;
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','reset card...');


%general property
prop.ires = 6;
prop.fPLL = 10.e9;
prop.tosc = 100.;
prop.blocksize = 65536;
prop.dmatimeout = 500;
prop.extclk = 0;
prop_ = libstruct('GENPROPERTY',prop);

%general settings
setting.range = fastSet.lengthSpectrum;
setting.bitshift = fastSet.bitshift;
setting.roimin = 0;
setting.roimax = setting.range;
setting.sweepmode = bin2dec('00000000000'); %falling
setting.dmamode = 0;
setting.syncout = 0;
setting.active = 1;
setting.fstchan = fastSet.startDelay;
setting.holdafter = fastSet.holdafter;
setting.cycles = 0;
setting_ = libstruct('GENSETTING',setting);

%
presetting.prena = bin2dec('100'); %sweep
presetting.swpreset = fastSet.sweepSum;
presetting.eventpreset = 0;
presetting.timepreset = 0;
presetting_ = libstruct('PRESETTING',presetting);


dacset.dac0 = v2nimdac(fastSet.startThresholdVolts,fastSet.startEdge);
dacset.dac1 = v2nimdac(fastSet.stopThresholdVolts,fastSet.stopEdge);
dacset_ = libstruct('DACSETTING',dacset);


digset.digio = 0;
digset.digval = 0;
digset_ = libstruct('DIGIOSETTING',digset);


datset.savedata = 0;
datset.fmt = 0;
datset.autoinc = 0;
datset_ = libstruct('DATSETTING',datset);


%set the card
calllib('p7889lib','HaltP89',1);
DATA = zeros(1,fastSet.lengthSpectrum);
DATA_ = libpointer('uint32Ptr',DATA);
calllib('p7889lib','SetVBDataP89',fastSet.numBoard,DATA_);
err0 = calllib('p7889lib','SetP89',fastSet.numBoard,prop_,setting_,presetting_,dacset_,digset_,datset_);
status.started = 0;
status_ = libstruct('ACQSTATUS',status);
calllib('p7889lib','StatusP89',fastSet.numBoard,status_);
% get(status_)

lm87.dac = 0;
lm87_ = libstruct('LM87VAL',lm87);
calllib('p7889lib','GetP89',fastSet.numBoard,prop_,setting_,presetting_,dacset_,digset_,datset_,lm87_);
% get(lm87_)


if(err0~=0)
    set(handle.status,'backgroundcolor','r');
    set(handle.status,'string','reset card...Error');
    fastSet.ready = 0;
else
    set(handle.status,'backgroundcolor','g');
    set(handle.status,'string','reset card...Done');
    fastSet.ready = 1;
end
pause(0.5);




function voltOut = v2nimdac(voltIn,edge)
    voltOutDec = floor((voltIn + 2.048)/4.096*65536+0.5);
    voltOutBin = dec2bin(voltOutDec,17);
    if(edge)
        voltOutBin(1) = '1';
    end
    voltOut = bin2dec(voltOutBin);

