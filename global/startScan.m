function startScan
global fastSet;
global scanSet;
global mainHandle;
handle = mainHandle.handles;

scanSet.stepNow = 1;
%create delay list
createDelayList;
%reset data array
resetData;
%reset plots
resetPlot(1);
%reset fast card
resetFast;
if(~fastSet.ready)
    return;
end
%define data pointer
status.started = 0;
status_ = libstruct('ACQSTATUS',status);
DATA = zeros(1,fastSet.lengthSpectrum);
DATA_ = libpointer('uint32Ptr',DATA);
calllib('p7889lib','SetVBDataP89',fastSet.numBoard,DATA_);
%move delay stage to the first position
moveTo(scanSet.delayList(scanSet.stepNow));



    
%% start scan
set(handle.status,'backgroundcolor','y');
while(scanSet.status && scanSet.stepNow<=scanSet.steps)
    %save start date
    scanSet.startDate(scanSet.stepNow,:) = clock;
    %start the acquiration
    calllib('p7889lib','StartP89',1,0);
    running = 1;
    %wait the acquiration
    while(running && scanSet.status)
        pause(1);
        running = calllib('p7889lib','MyCheckrunning',fastSet.numBoard);
        calllib('p7889lib','StatusP89',fastSet.numBoard,status_);
        statusNow = get(status_);
        %print message
        set(handle.status,'string',[num2str(scanSet.stepNow),'/',num2str(scanSet.steps),': ', ...
                                    num2str(scanSet.delayList(scanSet.stepNow),'%5.2f'), ...
                                    ', ',num2str(statusNow.sweeps), ...
                                    ', ',num2str(statusNow.runtime)]);
        drawnow;
    end
    %read out the data
    calllib('p7889lib','HaltP89',1);   
    scanSet.newTOF = double(get(DATA_,'value'));
    calllib('p7889lib','EraseP89',fastSet.numBoard);
    %save finish date
    scanSet.finishDate(scanSet.stepNow,:) = clock;

    %save reference
    if(mainHandle.autoRef)
        getReference; 
    end
    %update plots
    resetPlot(1);
    %go to next step
    scanSet.stepNow = scanSet.stepNow + 1;
    %move the delay stage to next position
    moveTo(scanSet.delayList(scanSet.stepNow));
    %
    str = {[num2str(mainHandle.stepTime*(scanSet.steps-scanSet.stepNow),'%5.1f'),' minutes left']};
    set(handle.restTime,'String',str);

end %while

if(scanSet.status>-1)
    calllib('p7889lib','HaltP89',1);
    saveData;
    set(handle.pushbutton3,'backgroundcolor','g');
    set(handle.pushbutton3,'String','Start');
    set(handle.status,'backgroundcolor','g');
    set(handle.status,'string','Scan finished!');
end










