function saveData
global fastSet;
global scanSet;
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Saving data...');


%save data
scanSet.comment = get(handle.edit13,'String');
save(fullfile(mainHandle.dataFolder,'scan.mat'),'fastSet','scanSet'); 
%save GUI to fig
saveas(handle.figure1,fullfile(mainHandle.dataFolder,'GUI.fig'));   
%link
disp(['<a href = "',mainHandle.dataFolder,'">',mainHandle.dataFolder,'</a>']);        
%
set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Saving data...Done!');