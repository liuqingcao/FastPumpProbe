function resetPlot(fix)
global mainHandle;
global scanSet;
handle = mainHandle.handles;

%convert tof to energy spectrum
scanSet.newSpec = interp1(scanSet.timeAxisKe(scanSet.tofBinMin:scanSet.tofBinMax), ...
                          scanSet.newTOF(scanSet.tofBinMin:scanSet.tofBinMax), ...
                          scanSet.keAxis);
                      
                      
                   
scanSet.tofCounts(scanSet.stepNow) = sum(scanSet.newTOF(scanSet.tofBinMin:scanSet.tofBinMax));
%save to data array
scanSet.allTOF(scanSet.stepNow,:) = scanSet.newTOF;
scanSet.allSpec(scanSet.stepNow,:) = scanSet.newSpec;

%plot tof
set(gcf,'CurrentAxes',handle.tofHandle);
cla(handle.tofHandle);
plot(scanSet.timeAxisReal,scanSet.newTOF,'r');
hold on;
plot(scanSet.timeAxisReal,scanSet.refTOF,'b');
hold off;
xlabel('Time of flight (ns)');
set(gca,'yscale','log');
if(fix)
   xlim(get(gca,'xlim')); 
end
drawnow;

%plot ke spectrum
set(gcf,'CurrentAxes',handle.keHandle);
cla(handle.keHandle);
plot(scanSet.keAxis,scanSet.newSpec,'r');
hold on;
plot(scanSet.keAxis,scanSet.refSpec,'b');
hold off;
xlabel('Kinetic energy (eV)');
set(gca,'yscale','log');
if(fix)
   xlim(get(gca,'xlim')); 
end
drawnow;

%plot counts
set(gcf,'CurrentAxes',handle.counts);
cla(handle.counts);
plot(scanSet.delayList,scanSet.tofCounts,'r');
xlabel('Total counts');
if(fix)
   xlim(get(gca,'xlim')); 
end
drawnow;

%plot all spectrum
set(gcf,'CurrentAxes',handle.allKe);
cla(handle.allKe);
imagesc(scanSet.delayList,scanSet.keAxis,scanSet.allSpec');
set(gca,'YDir','normal');
xlabel('Delay time (fs)');
ylabel('Kinetic energy (eV)');
if(fix)
   xlim(get(gca,'xlim')); 
   ylim(get(gca,'ylim')); 
end
drawnow;
