function createDataFolder
global scanSet;
global mainHandle;
handle = mainHandle.handles;
set(handle.status,'backgroundcolor','y');
set(handle.status,'string','Create data folder...');

%create data folder
if(~exist(mainHandle.baseFolder,'dir'))
    mkdir(mainHandle.baseFolder);
end
mainHandle.todayFolder = fullfile(mainHandle.baseFolder,date);
if(~exist(mainHandle.todayFolder,'dir'))
    mkdir(mainHandle.todayFolder);
end
%
mainHandle.dataFolder = fullfile(mainHandle.todayFolder,scanSet.project);
if(~strcmp(scanSet.project,'test'))
    if(~exist(mainHandle.dataFolder,'dir'))
        mkdir(mainHandle.dataFolder);
        scanSet.status = 1;
    else
        h = msgbox(['The project name is already exist, change to another one!']);
        uiwait(h);
        scanSet.status = -1;
        return;
    end
else
    if(~exist(mainHandle.dataFolder,'dir'))
        mkdir(mainHandle.dataFolder);
    end
    scanSet.status = 1;
end
set(handle.status,'backgroundcolor','g');
set(handle.status,'string','Create data folder...Done');
